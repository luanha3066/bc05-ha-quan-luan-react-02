import logo from "./logo.svg";
import "./App.css";
import ThayKinh from "./Thay_Kinh/ThayKinh.js";

function App() {
  return (
    <div className="App">
      <ThayKinh />
    </div>
  );
}

export default App;
