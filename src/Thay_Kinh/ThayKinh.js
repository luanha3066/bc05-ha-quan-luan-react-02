import React, { Component } from "react";
import model from "./glassesImage/model.jpg";
import g1 from "./glassesImage/g1.jpg";
import g2 from "./glassesImage/g2.jpg";
import g3 from "./glassesImage/g3.jpg";
import g4 from "./glassesImage/g4.jpg";
import g5 from "./glassesImage/g5.jpg";
import g6 from "./glassesImage/g6.jpg";
import g7 from "./glassesImage/g7.jpg";
import g8 from "./glassesImage/g8.jpg";
import g9 from "./glassesImage/g9.jpg";

export default class extends Component {
  state = {
    img: "./.png",
  };
  handleChangeGlass = (glass) => {
    this.setState({
      img: `./${glass}.png`,
    });
  };
  render() {
    return (
      <div className="background">
        <img
          style={{ position: "relative" }}
          id="modelImg"
          className=""
          src={model}
          alt=""
        />
        <img
          style={{ position: "absolute", top: "120px", left: "535px" }}
          className="w-25"
          src={this.state.img}
          alt=""
        />
        <div className="container bg-white ">
          <div className="row">
            <img
              onClick={() => this.handleChangeGlass("v1")}
              className="glasses"
              src={g1}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v2")}
              className="glasses mr-3"
              src={g2}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v3")}
              className="glasses mr-3"
              src={g3}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v4")}
              className="glasses mr-3"
              src={g4}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v5")}
              className="glasses mr-3"
              src={g5}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v6")}
              className="glasses mr-3"
              src={g6}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v7")}
              className="glasses mr-3"
              src={g7}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v8")}
              className="glasses mr-3"
              src={g8}
              alt=""
            />
            <img
              onClick={() => this.handleChangeGlass("v9")}
              className="glasses mr-3"
              src={g9}
              alt=""
            />
          </div>
        </div>
      </div>
    );
  }
}
